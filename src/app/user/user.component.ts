import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { User } from './user'; 
import { Router  } from '@angular/router';
import { APP } from '../constants'

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

    private apiEndpoint = APP.ApiEndpoint;
    id: number = Math.floor(Math.random() * (10 - 1 + 1)) + 1;
    profileInfo = new User(null, '', '', '', '', {});
    albums = [];

    constructor(private router: Router, private http: Http) {}

    ngOnInit() {
        //INICIAR COMPONENTE
        this.getUser();
        this.getAlbums();
    }

    //CARGAR USUARIO ALEATORIO
    getUser() {
        let url = `${this.apiEndpoint}/users?id=${this.id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => {
                this.profileInfo = new User(response.json()[0].id, response.json()[0].name, response.json()[0].username, response.json()[0].email, response.json()[0].phone, response.json()[0].company);
            })
            .catch(err => console.log(err));
    }

    //CARGAR ÁLBUMES USUARIO
    getAlbums() {
        let url = `${this.apiEndpoint}/albums?userId=${this.id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => {
                this.albums = response.json();
                console.log(this.albums);
            })
            .catch(err => console.log(err));
    }

    goToAlbumDetails(id) {
        this.router.navigate(['/album', id]);
    }

}