import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';

import { UserComponent } from './user/user.component';
import { AlbumComponent } from './album/album.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', component: UserComponent },
      { path: 'album/:id', component: AlbumComponent }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}