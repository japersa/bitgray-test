import { Component, OnInit, OnDestroy  } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ActivatedRoute  } from '@angular/router';
import {Location} from '@angular/common';
import { APP } from '../constants'


@Component({
    selector: 'app-album',
    templateUrl: './album.component.html',
    styleUrls: ['./album.component.css']
})

export class AlbumComponent implements OnInit, OnDestroy  {
    private apiEndpoint = APP.ApiEndpoint;
    id: number;
    private sub: any;
    photos = [];


    constructor(private route: ActivatedRoute, private http: Http, private _location: Location) {}

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id'];
            this.getPhotos();
        });
    }
    
    ngOnDestroy() {
      this.sub.unsubscribe();
    }


    //VOLVER ATRAS
    goToBack() {
        this._location.back();
    }


    //CARGAR FOTOS ALBUM
    getPhotos() {
        let url = `${this.apiEndpoint}/photos?albumId=${this.id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => {
                this.photos = response.json();
            })
            .catch(err => console.log(err));
    }


}