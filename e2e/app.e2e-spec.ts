import { BitgrayTestPage } from './app.po';

describe('bitgray-test App', function() {
  let page: BitgrayTestPage;

  beforeEach(() => {
    page = new BitgrayTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
